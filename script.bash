hax(){
    if [ $1 == "start" ] 
    then
        export PS1="\h:\W h4ck3rm4n\$ "
        node ~/hax.js
    elif [ $1 == "mode" ] 
    then
        export PS1="\h:\W h4ck3rm4n\$ "
        osascript -e "tell application \"Terminal\" to set current settings of window 1 to settings set \"hax\""
    elif [ $1 == "init" ] 
    then
        export PS1="\h:\W h4ck3rm4n\$ "
        osascript -e "tell application \"Terminal\" to set current settings of window 1 to settings set \"hax\""
        node ~/hax.js
    elif [ $1 == "exit" ] 
    then
        export PS1="\h:\W \u\$ "
        osascript -e "tell application \"Terminal\" to set current settings of window 1 to settings set \"Silver Aerogel\""
    elif [ $1 == "info" ] 
    then
        screenfetch
    fi
}